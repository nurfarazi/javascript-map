var express = require('express');

var mongojs = require('mongojs')
var bodyParser = require('body-parser');

var db = mongojs('contactlist', ['contactlist'])
var ObjectId = mongojs.ObjectId;
var app = express();
var map;
var fs = require('fs');






app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());



app.get('/contactlist', function (req, res) {

	console.log("contact route connected ");

	db.contactlist.find(function (err, docs) {

		res.json(docs);
	})
});

app.post('/contactlist', function (req, res) {

	db.contactlist.insert(req.body, function (err, doc) {

		res.json(doc);
	})

});

app.delete('/contactlist/:id', function (req, res) {

	var tempid = req.params.id;

	console.log(tempid);

	db.contactlist.remove({
		"_id": ObjectId(tempid)
	}, function (err, docs) {
		console.log("Hello" + docs);
		res.json(docs);
	})
});


app.listen(3000);

console.log('Server is listening to http://localhost/ on port 3000…');
