var map;



function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 3,
		center: {
			lat: 0,
			lng: -180
		},
		mapTypeId: google.maps.MapTypeId.TERRAIN
	});

	var flightPlanCoordinates = [{
		lat: 34.772,
		lng: -122.214
			}, {
		lat: 21.291,
		lng: -157.821
			}, {
		lat: -18.142,
		lng: 178.431
			}, {
		lat: -27.467,
		lng: 153.027
			}];


	var symbolThree = {
		path: 'M0,0 L4,2 0,4',
		strokeColor: '#292',
		scale: 4,
		strokeColor: 'green',
		strokeWeight: 3
	};
	var ArrowSymbol = {
		path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
		scale: 4,
		rotation: 45,

	};



	var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		geodesic: true,
		strokeColor: '#FF0000',
		strokeOpacity: 1.0,
		strokeWeight: 2
	});


	for (var i = 0; i < flightPlanCoordinates.length; i++) {

		var marker = new google.maps.Marker({
			position: flightPlanCoordinates[i],
			icon: ArrowSymbol,
			map: map,
			title: 'Hello World!'
		});

	}

	marker.setMap(map);
	flightPath.setMap(map);

	//	console.log("refresheeeeeed");
	//	setTimeout(initMap, 10000);
	getLocation();

}

function getLocation() {

	setInterval(function () {

		console.log("refreshed");
		initMap();

	}, 10000)

}
